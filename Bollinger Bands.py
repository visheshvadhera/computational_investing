import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

def bollinger():
    ls_symbols = ['MSFT']
    dt_start = dt.datetime(2010, 1, 1)
    dt_end = dt.datetime(2010, 12, 31)

    dt_timeofday = dt.timedelta(hours=16)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)

    #Price Dataframe
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
    ls_keys = ['close']
    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))
    na_price = d_data['close'].values
    df_boll = pd.DataFrame(index=ldt_timestamps, columns=ls_symbols)
    df_boll[ls_symbols] = na_price

    #Rolling Mean Over 20 days
    RollingMean = pd.rolling_mean(df_boll, 20)
    df_boll['Mean'] = RollingMean

    #Rolling Std Deviation Over 20 days
    RollingStdDev = pd.rolling_std(df_boll[ls_symbols], 20)
    df_boll['Std_Dev'] = RollingStdDev

    #Upper Bollinger
    df_boll['Upper Bollinger'] = df_boll['Mean'] + RollingStdDev

    #Lower Bollinger
    df_boll['Lower Bollinger'] = df_boll['Mean'] - RollingStdDev

    #Bollinger Value from -1 to 1
    df_boll['Bollinger Value'] = (df_boll[ls_symbols] - df_boll['Mean'])/df_boll['Std_Dev']
    
    #df_boll.plot()
    #plt.show()
    return df_boll

    

pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 600)
pd.set_option('display.max_columns', 600)
pd.set_option('display.width', 1000)

print bollinger()

