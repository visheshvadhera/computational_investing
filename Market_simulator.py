import csv
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import itertools


import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da


def readCSV():
    symbols = []
    date = []

    reader = csv.reader(open('C:\Users\Vishesh\Videos\MOOC\Computational Investing\orders-short.csv', 'rU'), delimiter=',')
    for row in reader:
        symbols.append(row[3])
        date.append(row[0:3])
            
    symbols = list(set(symbols))
    return symbols, date 

def market():
    date = readCSV()[1]
    symbols = ['AAPL', 'IBM'] #readCSV()[0]
    
    dt_start = dt.datetime(int(date[0][0]), int(date[0][1]), int(date[0][2]))
    dt_last = dt.datetime(int(date[len(date)-1][0]), int(date[len(date)-1][1]), int(date[len(date)-1][2]))

    dt_end = dt_last + dt.timedelta(days=1)         #End Date Offset by 1 day 
                                                    #to read the close for last day

    dt_timeofday = dt.timedelta(hours=16)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)

    #Price Dataframe
    c_dataobj = da.DataAccess('Yahoo')
    ls_keys = ['close']
    ldf_data = c_dataobj.get_data(ldt_timestamps, symbols, ls_keys)
    df = pd.DataFrame(ldf_data)
    d_data = dict(zip(ls_keys, ldf_data))
    na_price = d_data['close'].values
    df_price = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_price[symbols] = na_price 

    #Trade Dataframe
    df_trade = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_trade[symbols] = 0
    
    reader = csv.reader(open('C:\Users\Vishesh\Videos\MOOC\Computational Investing\orders-short.csv', 'rU'), delimiter=',')
    for row in reader:
        date = pd.datetime(int(row[0]), int(row[1]), int(row[2]), 16, 00, 00)
        if row[4]=='Buy':
            df_trade[row[3]][date] += int(row[5])
        elif row[4]=='Sell':
            df_trade[row[3]][date] -= int(row[5])

    
    #Holdings Dataframe
    df_holdings = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_holdings = df_trade.apply(np.cumsum)

    #Funds Dataframe
    df_fund = df_holdings*df_price

    #Cost DataFrame
    df_cost = df_trade*df_price

    #Cash Dataframe
    cash = ['Cash']
    df_cash = pd.DataFrame(index=ldt_timestamps, columns=cash)
    df_cash['Cash']=0
    df_cash['Cash'][0] = 1000000
    df_cash = (df_cash-df_cost.sum(axis=1)).apply(np.cumsum)
    
    #Value Dataframe
    df_value = df_fund.sum(axis=1) + df_cash 

    # Writing them to a CSV File
    writer = csv.writer(open('C:\Users\Vishesh\Videos\MOOC\Computational Investing\orders-value.csv', 'wb'), delimiter=',')
    for row_index in df_value.index:
        print row_index
        print df_value[row_index]
        row_to_enter = ['JUNK', 'DATA']
        writer.writerow(row_to_enter)
    
    return df_value


print market()
    
     



