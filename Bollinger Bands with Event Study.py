import pandas as pd
import numpy as np
import math
import copy
import csv
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep

def find_events(ls_symbols, d_data):
    ''' Finding the event dataframe '''
    df_close = d_data['close']
    ts_market = df_close['SPY']

    # Creating an empty dataframe
    df_events = copy.deepcopy(df_close)
    df_events = df_events * np.NAN

    # Time stamps for the event range
    ldt_timestamps = df_close.index

    # Creating 3 Empty dataframes for rolling mean, rolling Std Dev and Bollinger values
    df_mean = copy.deepcopy(df_close)
    df_mean = df_mean * np.NAN
    df_stdDev = copy.deepcopy(df_close)
    df_stdDev = df_stdDev * np.NAN
    df_bollinger = copy.deepcopy(df_close)
    df_bollinger = df_bollinger * np.NAN

 
    for symbol in ls_symbols:
        #Rolling Mean Over 20 days for stocks
        RollingMean = pd.rolling_mean(df_close[symbol], 20)
        df_mean[symbol] = RollingMean

        #Rolling Std Deviation Over 20 days stocks
        RollingStdDev = pd.rolling_std(df_close[symbol], 20)
        df_stdDev[symbol] = RollingStdDev

        #Bollinger Value from -1 to 1
        df_bollinger[symbol] = (df_close[symbol] - df_mean[symbol])/df_stdDev[symbol]

    print "Finding Events"

    # Write the events to this file
    b = open('C:\Users\Vishesh\Videos\MOOC\Computational Investing\orders-homework7.csv', 'wb')
    a = csv.writer(b, delimiter=',')

    for s_sym in ls_symbols[0:len(ls_symbols)-1]:
        for i in range(20, len(ldt_timestamps)):
            # Calculating the bollinger band for this timestamp
            bollinger_sym_today = df_bollinger[s_sym].ix[ldt_timestamps[i]]
            bollinger_sym_yest = df_bollinger[s_sym].ix[ldt_timestamps[i - 1]]
            bollinger_market_today = df_bollinger['SPY'].ix[ldt_timestamps[i]]

            # Event is found if the symbol's Bollinger Band is lower than -2 while the
            # market is higher than  1
            if bollinger_sym_today < -2.0 and bollinger_sym_yest >= -2.0 and bollinger_market_today>=1.1:
                df_events[s_sym].ix[ldt_timestamps[i]] = 1
                buy_date = str(ldt_timestamps[i])
                buy_string = buy_date[0:4] + ','  + buy_date[5:7] + ',' + buy_date[8:10] + ',' + s_sym + ',' + 'Buy' + ',' + str(100) + ','
                if i+5>= len(ldt_timestamps) - 1:
                    sell_date = str(ldt_timestamps[len(ldt_timestamps)-1])
                    sell_string = sell_date[0:4] + ','  + sell_date[5:7] + ',' + sell_date[8:10] + ',' + s_sym + ',' + 'Sell' + ',' + str(100) + ','
                else:
                    sell_date = str(ldt_timestamps[i+5])
                    sell_string = sell_date[0:4] + ','  + sell_date[5:7] + ',' + sell_date[8:10] + ',' + s_sym + ',' + 'Sell' + ',' + str(100) + ','
                data = [[buy_string],
                        [sell_string]]
                a.writerows(data)

    return df_events


if __name__ == '__main__':
    dt_start = dt.datetime(2008, 1, 1)
    dt_end = dt.datetime(2009, 12, 31)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))

    dataobj = da.DataAccess('Yahoo', cachestalltime=0)
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')

    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))

    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method='ffill')
        d_data[s_key] = d_data[s_key].fillna(method='bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    df_events = find_events(ls_symbols, d_data)
    print "Creating Study"
    #ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
    #           s_filename='Bollinger_EventStudy_2012.pdf', b_market_neutral=True, b_errorbars=True,
    #          s_market_sym='SPY')
