# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import itertools

def values():
    #array from values in steps of 0.1 from 0 to 1
    possible_values = [round(x * 0.1, 1) for x in range(11)]
    #cartesian product of possible_values with itself repeated four times
    #this is equivalent to the nested loops used by some
    all_allocations = itertools.product(possible_values, repeat=4)
    #filter out only those where the four allocations sum to 1 (100%)
    full_allocations = [list(allocation) for allocation in all_allocations if sum(allocation) == 1]
    #returns an array of arrays, where each is a set of allocations

    na_allocation = np.array(full_allocations)
    na_full_allocation = na_allocation.reshape(len(full_allocations), len(full_allocations[0]))
    
    return na_full_allocation

def simulate():

    dt_start = dt.datetime(2011, 1, 1)
    dt_end = dt.datetime(2011, 12, 31)
    ls_symbols = ['BRCM', 'TXN', 'IBM', 'HNZ']
    dt_timeofday = dt.timedelta(hours=16)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)

    c_dataobj = da.DataAccess('Yahoo')
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))
    #allocation = np.array( [0.4, 0.4, 0.0, 0.2] )
    #act_alloc = allocation.reshape((4,1))

    na_price = d_data['close'].values

    na_normalized_price = na_price / na_price[0, :]

    #na_daily_return = np.dot(na_normalized_price, act_alloc)
    #na_daily_return_f = tsu.returnize0(np.array(na_daily_return.copy()))    

    
    # Standard Deviation
    #stdDev = np.std(na_daily_return_f)
    
    # Average Return
    #avgRet = np.average(na_daily_return_f)

    # Sharpe Ratio
    #sharpe = (np.mean(na_daily_return_f)*math.sqrt(252))/stdDev

    # Cumulative Return
    #cum_ret = na_daily_return[251]

    # Finding the best possible Portfolio
    possible_alloc = values()
    na_normalized_price_2 = np.array(na_normalized_price.copy())
    optimum_sharpe = 0.0
    best_allocation = np.array([0, 0, 0, 0])
    i = 0
    while i<len(possible_alloc):
        portfolio = np.dot(na_normalized_price_2, possible_alloc[i].reshape(4,1))
        na_daily_return_2 = tsu.returnize0(np.array(portfolio.copy()))
        sharpe_2 = (np.mean(na_daily_return_2)*math.sqrt(252))/np.std(na_daily_return_2)
        if sharpe_2>optimum_sharpe:
            optimum_sharpe = sharpe_2
            best_allocation = possible_alloc[i]
        i = i+1

    return optimum_sharpe, best_allocation

print simulate()
    


    






    
