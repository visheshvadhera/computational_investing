import csv
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import itertools


import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da


def LoadOrders(file_name):
  Orders = pd.read_csv(file_name, parse_dates = [[0, 1, 2]], header = None, index_col = [0])
  Orders.columns = ['Symbol', 'Command', 'Value', 'Dummy']
  Orders = Orders.sort_index()
  
  return Orders


def market():
    orders = LoadOrders('C:\Users\Vishesh\Videos\MOOC\Computational Investing\orders-homework7.csv') 

    # Extracting Symbols
    symbols =  orders.Symbol.drop_duplicates().tolist()

    #Extracting Dates
    dates = orders.index.tolist()
    first_date = str(dates[0])
    last_date = str(dates[len(dates)-1])
    
    
    dt_start = dt.datetime(int(first_date[0:4]), int(first_date[5:7]), int(first_date[8:10]))
    dt_last = dt.datetime(int(last_date[0:4]), int(last_date[5:7]), int(last_date[8:10]))

    dt_end = dt_last + dt.timedelta(days=1)         #End Date Offset by 1 day 
                                                    #to read the close for last day

    dt_timeofday = dt.timedelta(hours=16)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)

    #Price Dataframe
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
    ls_keys = ['close']
    ldf_data = c_dataobj.get_data(ldt_timestamps, symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))
    na_price = d_data['close'].values
    df_price = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_price[symbols] = na_price 

    #Trade Dataframe
    df_trade = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_trade[symbols] = 0
    
    
    for k in range(0, len(dates)):
        date = pd.datetime(int(str(dates[k])[0:4]), int(str(dates[k])[5:7]), int(str(dates[k])[8:10]))
        date_timestamp = date + dt.timedelta(hours=16)
        if orders.ix[k, 'Command']=='Buy':
            df_trade.loc[date_timestamp, orders.ix[k, 'Symbol']] += int(orders.ix[k, 'Value'])
        elif orders.ix[k, 'Command']=='Sell':
            df_trade.loc[date_timestamp, orders.ix[k, 'Symbol']] -= int(orders.ix[k, 'Value']) 

    
    #Holdings Dataframe
    df_holdings = pd.DataFrame(index=ldt_timestamps, columns=symbols)
    df_holdings = df_trade.apply(np.cumsum)

    #Funds Dataframe
    df_fund = df_holdings*df_price

    #Cost DataFrame
    df_cost = df_trade*df_price

    #Cash Dataframe
    cash = ['Cash']
    df_cash = pd.DataFrame(index=ldt_timestamps, columns=cash)
    df_cash['Cash']=0
    df_cash['Cash'][0] = 100000
    df_cash = (df_cash-df_cost.sum(axis=1)).apply(np.cumsum)
    
    #Value Dataframe
    df_value = df_fund.sum(axis=1) + df_cash 

    #Statistics about the portfolio
    na_value = df_value.values
    na_daily_return = tsu.returnize0(na_value.copy())

    #Standard Deviation
    stdDev = np.std(na_daily_return)

    #Average Return
    avgRet = np.average(na_daily_return)

    #Sharpe Ratio
    sharpe = (np.mean(na_daily_return)*math.sqrt(252))/stdDev

    #Cumulative Return
    cum_ret = na_value[len(na_value)-1]/na_value[0]
    
    
    
    return sharpe

pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 600)
pd.set_option('display.max_columns', 600)
pd.set_option('display.width', 1000)

print market()
    
     



